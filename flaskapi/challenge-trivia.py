#!/usr/bin/python3

from flask import Flask
from flask import redirect
from flask import url_for
from flask import request
from flask import render_template

app = Flask(__name__)

def get_trivia_answer():
    return "4" # lets just use a string for the exercise...

def get_trivia_question():
    return "What is 2+2?"

@app.route("/") 
def start():
    q = get_trivia_question()
    return render_template("ask_trivia.html", question=q) 

@app.route("/correct")
def success():
    return "Congrats! You answered correctly"


@app.route("/answer", methods = ["POST"])
def answer():
    if request.method == "POST":
        if request.form.get("answer"): 
            response = request.form.get("answer") 
        else: 
            response = None
        
        if response == get_trivia_answer():
            return redirect(url_for("success")) 

    return redirect(url_for("start"))


if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224)

