#!/usr/bin/python3

from flask import Flask
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from flask import render_template

app = Flask(__name__)

app.secret_key= "random random RANDOM!"
SUPER_SECRET_LOGIN_KEY = "42"

groups = [{"hostname": "hostA","ip": "192.168.30.22", "fqdn": "hostA.localdomain"},
          {"hostname": "hostB", "ip": "192.168.30.33", "fqdn": "hostB.localdomain"},
          {"hostname": "hostC", "ip": "192.168.30.44", "fqdn": "hostC.localdomain"}]


@app.route("/")
def index():
    if "logged_in" in session and session['logged_in'] == True:
        return render_template("edit_hosts.j2", groups=groups)
    else:
        return redirect("/login")

@app.route("/edit", methods=["GET","POST"])
def edit():
    if request.method == "POST" and "logged_in" in session and session['logged_in'] == True:
        hostname = request.form.get("hostname")
        ip = request.form.get("ip")
        fqdn = request.form.get("fqdn")
        groups.append({"hostname": hostname, "ip": ip, "fqdn": fqdn})

    return redirect("/")

@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        passkey = request.form.get("passkey")
        if passkey == SUPER_SECRET_LOGIN_KEY:
            session['logged_in'] = True
            return redirect("/")
        else:
            return redirect("/login")
    else:
        return render_template("challenge_login.j2")

@app.route("/logout", methods=["GET","POST"])
def logout():
    session['logged_in']=False
    return redirect("/login")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)
