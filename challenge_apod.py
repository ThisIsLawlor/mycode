import requests
import pprint

url = "https://api.nasa.gov/planetary/apod"
api_key = "DEMO_KEY"

def main():
    requested_date = get_date_from_user()
    requested_is_hd = get_is_hd_from_user()
    apod_info = get_apod_info(requested_date, requested_is_hd)
    print(f"Date:{apod_info['date']}")
    print(f"Title:{apod_info['title']}")
    print(f"Description:{apod_info['explanation']}")

    get_apod_image(apod_info, requested_is_hd)


def get_date_from_user():
    #return "2022-10-12"
    return input("date (YYYY-MM-DD) :")

def get_is_hd_from_user():
    return input("HD? (enter y for yes):") == "y"

def get_apod_info(date, hd):
    response = requests.get( url + f"?api_key={api_key}&date={date}&hd={hd}")
    #pprint.pprint(response.json())
    return response.json()

def get_apod_image(apod_info, hd):
    if hd:
        request_url = apod_info['hdurl']
    else:
        request_url = apod_info['url']

    response = requests.get( request_url )
    open('/home/student/static/'+f"{apod_info['title']}.jpg", 'wb').write(response.content)


if __name__ == "__main__":
    main()
