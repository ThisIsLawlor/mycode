#!/usr/bin/python3
"""Alta3 Research - Exploring OpenAPIs with requests"""
# documentation for this API is at
# https://anapioficeandfire.com/Documentation

import requests
import pprint

AOIF_CHAR = "https://www.anapioficeandfire.com/api/characters/"

def main():
        ## Ask user for input
        got_charToLookup = input("Pick a number between 1 and 1000 to return info on a GoT character! " )

        ## Send HTTPS GET to the API of ICE and Fire character resource
        gotresp = requests.get(AOIF_CHAR + got_charToLookup)

        ## Decode the response
        got_dj = gotresp.json()
        name = got_dj['name'] 
        
        if len(got_dj['allegiances']) > 0:
            print(f"{name} is loyal to house(s):")
        else:
            print(f"{name} is not loyal to any house")

        for house_url in got_dj['allegiances']:
            house = requests.get(house_url).json()
            print(house['name'])

        #pprint.pprint(got_dj)
        if len(got_dj['books']) > 0:
            print(f"They appear in book(s):")

        for book_url in got_dj['books']:
            book = requests.get(book_url).json()
            print(book['name'])

if __name__ == "__main__":
        main()

