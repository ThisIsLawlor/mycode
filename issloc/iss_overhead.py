#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests

def get_location():
    # just return hard-coded value for now
    return ({"lat":47.6, "lon":-122.3})

def main():
    http://api.open-notify.org/iss-now.json
    base_url = "http://api.open-notify.org/iss-pass.json"
    location = get_location()
    print(base_url + f"?lat={location['lat']}&lon={location['lon']}")
    resp = requests.get(base_url + f"?lat={location['lat']}&lon={location['lon']}") 
    data = resp.json()
    print("The ISS will be overhead at:")
    for t in data['response']:
        print(f"{t['risetime']} for {t['duration']}")



if __name__ == "__main__":
    main()

